# uvicorn sample2:app --host 0.0.0.0 --port 80

from fastapi import FastAPI, status, HTTPException
from dataclasses import dataclass
import logging

app = FastAPI()

logger = logging.getLogger("fastapi")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s - %(levelname)s - jobid(%(jobid)s) : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


@dataclass
class Image:
    # entity sample
    name: str


class ImageNotFoundError(Exception):
    pass


class ImageRepository:
    @staticmethod
    def get_all():
        return ["test_image1", "test_image2"]

    @classmethod
    def find_by_name(cls, name: str):
        images = cls.get_all()

        for image_name in images:
            if image_name == name:
                return Image(image_name)

        raise ImageNotFoundError(f"{name} is not found in fileserver")


@app.get("/images/{image_name}", status_code=status.HTTP_200_OK)
def get_image(image_name):
    try:
        image = ImageRepository.find_by_name(image_name)
        return image
    except ImageNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.args[0]  # 別Exceptionの例外を
        )
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
