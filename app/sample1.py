# uvicorn sample1:app --host 0.0.0.0 --port 80
import contextvars
import logging
import time
from datetime import datetime
from typing import Optional

from fastapi import FastAPI, BackgroundTasks
from pydantic import BaseModel, validator

app = FastAPI()
jobid_contextvar = contextvars.ContextVar("jobid", default="main")


class ContextFilter(logging.Filter):
    # refs
    # https://docs.python.org/ja/3/howto/logging-cookbook.html#using-filters-to-impart-contextual-information
    # https://medium.com/gradiant-talks/identifying-fastapi-requests-in-logs-bac3284a6aa

    def filter(self, record):
        # add new vars to record
        record.jobid = jobid_contextvar.get()

        # Apply filter to the record and return True
        # https://docs.python.org/ja/3/library/logging.html#logging.Logger.filter
        return True


logger = logging.getLogger("fastapi")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s - %(levelname)s - jobid(%(jobid)s) : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)
fillter = ContextFilter()
logger.addFilter(fillter)


def sleep(n=600):
    logger.info(f"wait for {n}s")
    time.sleep(n)
    logger.info(f"finish (wait for {n}s)")


class PostData(BaseModel):
    date: Optional[datetime] = None
    host_fqdn: str
    image_name: str
    mode: Optional[str] = None

    @validator("date", always=True)
    def set_datetime(cls, value):
        return f"{datetime.now():%Y%m%d%H%M%S}"

    @property
    def jobid(self):
        return f"{self.host_fqdn}-{self.date}"


@app.get("/")
def read_root():
    logger.info("root-dir")
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@app.post("/simple_wait/{time}")
def simple_wait(time: int):
    logger.info("simple_wait")
    sleep(time)


@app.post("/background_wait/{time}")
def background_wait(time: int, bg_tasks: BackgroundTasks):
    logger.info("background_wait")
    bg_tasks.add_task(sleep, time)
    return {"msg": f"wait for {time}s at background"}


@app.post("/install/")
def install(post_data: PostData):
    # context vars store, and access context-local state such as thread.local
    jobid_contextvar.set(post_data.jobid)
    logger.info("testdayo-")
    logger.info(post_data)
    return post_data
