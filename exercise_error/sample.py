# uvicorn sample:app --host 0.0.0.0 --port 80

from fastapi import FastAPI, status, HTTPException
from dataclasses import dataclass
import logging

app = FastAPI()

logger = logging.getLogger("fastapi")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s - %(levelname)s - jobid(%(jobid)s) : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


@dataclass
class Image:
    # entity sample
    name: str


class ImageNotFoundError(Exception):
    pass


class ShellCmdError(Exception):
    pass


@app.get("/error-sample1/images/{image_name}", status_code=status.HTTP_200_OK)
def get_image_error1(image_name):
    """
    patter1: 別クラスの例外をcatchしてそのまま伝える方法(404)
    return:
        {"detail": "testdayo-"}
    """
    try:
        raise ImageNotFoundError("testdayo-")
    except ImageNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.args[0]  # 別Exceptionの例外msgをそのまま使う。
        )


@app.get("/error-sample2/images/{image_name}", status_code=status.HTTP_200_OK)
def get_image_error2(image_name):
    """
    patter2: 別クラスの例外をcatchしてそのまま伝える方法(500)
    return:
        {"detail": "Failed shell cmd: genisoimage -relaxed-filenames ...")
    """
    try:
        cmd = "genisoimage -relaxed-filenames -J -R -o custom_esxi.iso -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e efiboot.img -no-emul-boot /esxi_cdrom"
        raise ShellCmdError(f"Failed shell cmd: {cmd}")

    except ImageNotFoundError as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=e.args[0]  # 別Exceptionの例外msgをそのまま使う。
        )

    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=e.args[0]  # 別Exceptionの例外msgをそのまま使う。
        )
