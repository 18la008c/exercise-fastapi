# uvicorn sample4:app --host 0.0.0.0 --port 80
# dependency injenction at decorator
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/dependencies-in-path-operation-decorators/

from fastapi import Depends, FastAPI, Header, HTTPException

app = FastAPI()
# app = FastAPI(dependencies=[Depends(verify_token), Depends(verify_key)])
# FastAPI全体に、Dependsの設定もできる(global dependencies)
# refs https://fastapi.tiangolo.com/ja/tutorial/dependencies/global-dependencies/


async def verify_token(x_token: str = Header(default=None)):
    # Header classを使用することで、headerを自動的に取得してくれる。
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


async def verify_key(x_key: str = Header(default=None)):
    if x_key != "fake-super-secret-key":
        raise HTTPException(status_code=400, detail="X-Key header invalid")
    return x_key


# Dependsが返り値を必要ない時は、decoratorに記載できる。そうすれば関数には返り値が渡されない。(関数の実行はされる。)
@app.get("/items/", dependencies=[Depends(verify_token), Depends(verify_key)])
async def read_items():
    return [{"item": "Foo"}, {"item": "Bar"}]


@app.get("/headers/")
def read_headers(x_token: str = Header(default=None)):
    """
    curl -X 'GET' \
        'http://localhost/headers/' \
        -H 'accept: application/json' \
        -H 'x-token: abc'
    {
        "token": "abc"
    }
    """
    return {"token": x_token}


"""
curl -X 'GET' \
  'http://localhost/items/' \
  -H 'accept: application/json' \
  -H 'x-token: fake-super-secret-token' \
  -H 'x-key: fake-super-secret-key'

[
  {
    "item": "Foo"
  },
  {
    "item": "Bar"
  }
]

curl -X 'GET' \
  'http://localhost/items/' \
  -H 'accept: application/json' \
  -H 'x-token: aaa' \
  -H 'x-key: fake-super-secret-key'

{
  "detail": "X-Token header invalid"
}
"""
