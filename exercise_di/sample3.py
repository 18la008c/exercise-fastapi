# uvicorn sample3:app --host 0.0.0.0 --port 80
# dependency injenction (nested dependency)
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/sub-dependencies/

from typing import Union

from fastapi import Cookie, Depends, FastAPI

app = FastAPI()


def query_extractor(q: Union[str, None] = None):
    return q


def query_or_cookie_extractor(
    q: str = Depends(query_extractor),
    last_query: Union[str, None] = Cookie(default=None),
):
    # qがNoneなら、cookieに保存されている前回のqが使用されるらしいが、このコードだけではできない模様
    # cookieへの保存がどこで必要らしい
    if not q:
        return last_query
    return q


@app.get("/items/")
async def read_query(query_or_default: str = Depends(query_or_cookie_extractor)):
    # Depends → query_or_cookie → Depends → query_extractor
    # のように、ネストされた依存関係を構築できる
    return {"q_or_cookie": query_or_default}
