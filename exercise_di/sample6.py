# uvicorn sample6:app --host 0.0.0.0 --port 80
# dependency injenction with yield & Exception
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/dependencies-with-yield/
from fastapi import Depends, FastAPI, HTTPException, status

app = FastAPI()


class DBConnectException(Exception):
    pass


class ErrorDBSession:
    # dbに接続不可を模擬する。
    def __init__(self):
        raise DBConnectException

    def close(self):
        print("close")

    def read(self, name: str) -> str:
        return name


async def get_db():
    try:
        db = ErrorDBSession()
    except DBConnectException:
        # path operationの前にdiの関数は実行されるので、「diのyield前に起きる例外」はここで定義が必要！
        # 必ず「yield後に起きる例外」と区別するために、 yieldのtry-finallyと分けること！！
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    try:
        # 以下では、yield後の処理は、api-response実行後に実施されるとある。だからyield後のraise HTTPExceptionは無意味
        # しかし、fastapi-0.75.2では、api-responseの前に、yieldが実行されているように見える。
        # ちょっと不明なので、 yieldのtry-finallyでは、exceptを使用しないべき！！
        # https://fastapi.tiangolo.com/ja/tutorial/dependencies/dependencies-with-yield/#dependencies-with-yield-and-httpexception
        yield db
    finally:
        db.close()


@app.get("/books/{name}")
def read_books(name: str, db=Depends(get_db)):  # get_dbで必ず失敗する。
    """
    curl -X 'GET' \
        'http://localhost/books/hiroki' \
        -H 'accept: application/json'
    {
        "detail": "Internal Server Error"
    }
    """
    book = db.read(name)
    return {"book_title": book}
