# uvicorn sample5:app --host 0.0.0.0 --port 80
# dependency injenction with yield
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/dependencies-with-yield/
from fastapi import Depends, FastAPI

app = FastAPI()


class DBSession:
    def __init__(self):
        pass

    def close(self):
        print("close")

    def read(self, name: str) -> str:
        return name


async def get_db():
    db = DBSession()

    try:
        yield db  # yieldを使うことで、api-response返す前に実行される。
    finally:  # api-responseを返した後に、finallyが実行される。 (つまりapi-request毎にsessionを閉じることができる。)
        db.close()


@app.get("/books/{name}")
def read_books(name: str, db=Depends(get_db)):
    """
    curl -X 'GET' \
        'http://localhost/books/adas' \
        -H 'accept: application/json'
    {
        "book_title": "adas"
    }
    """
    book = db.read(name)
    return {"book_title": book}
