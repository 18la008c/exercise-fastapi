# uvicorn sample2:app --host 0.0.0.0 --port 80
# dependency injenction by class
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/classes-as-dependencies/

from typing import Union

from fastapi import Depends, FastAPI

app = FastAPI()


fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]


class CommonQueryParams:
    # classにすると？
    # dictと違い、type-hintsが使えるようになる。
    # dict = {"q": q, "skip": skip, "limit": limit} → dictの中身1つ1つのtypeがわからない。
    # class = q: str, skip: int, limit: int → type-hintsが使える → swaggerにも反映できる
    def __init__(self, q: Union[str, None] = None, skip: int = 0, limit: int = 100):
        self.q = q
        self.skip = skip
        self.limit = limit


@app.get("/items/")
# async def read_items(commons: CommonQueryParams = Depends()): # type-hintsをつけた場合はDepends()でも良い。
async def read_items(commons: CommonQueryParams = Depends(CommonQueryParams)):
    response = {}
    if commons.q:
        response.update({"q": commons.q})
    items = fake_items_db[commons.skip: commons.skip + commons.limit]
    response.update({"items": items})
    return response

"""
curl -X 'GET' \
  'http://localhost/items/?skip=0&limit=100' \
  -H 'accept: application/json'

{
  "items": [
    {
      "item_name": "Foo"
    },
    {
      "item_name": "Bar"
    },
    {
      "item_name": "Baz"
    }
  ]
}
"""
