# uvicorn sample:app --host 0.0.0.0 --port 80
# dependency injenction by function
# https://fastapi.tiangolo.com/ja/tutorial/dependencies/
from typing import Union

from fastapi import Depends, FastAPI

app = FastAPI()


async def common_parameters(
    q: Union[str, None] = None, skip: int = 0, limit: int = 100
):
    return {"q": q, "skip": skip, "limit": limit}


@app.get("/items/")
async def read_items(commons: dict = Depends(common_parameters)):
    return commons


@app.get("/users/")
async def read_users(commons: dict = Depends(common_parameters)):
    return commons


@app.get("/books/{name}")
def read_books(name, id: str, isbn: str):
    # nameはpath-parameter
    # id, isbnはquery-parameterになる。
    return name + id
