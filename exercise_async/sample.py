# async/awaitの間違った使い方1
# ただasyncを使っただけでは、同期処理のままで平行処理されない
import asyncio
import time
import logging

logger = logging.getLogger("main")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
formatter = logging.Formatter(
    "%(asctime)s - %(levelname)s : %(message)s")
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)


async def hello_world1():
    logger.info("wait5")
    time.sleep(5)
    logger.info("Hello World!")


async def hello_world2():
    logger.info("wait10")
    time.sleep(10)
    logger.info("Hello World!")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(hello_world1())
    loop.run_until_complete(hello_world2())

# asyncで定義するだけだと、全然平行処理じゃない!!
"""
Return:
    2022-05-08 00:23:44,854 - INFO : wait5
    2022-05-08 00:23:49,859 - INFO : Hello World!
    2022-05-08 00:23:49,860 - INFO : wait10
    2022-05-08 00:23:59,861 - INFO : Hello World!
"""
